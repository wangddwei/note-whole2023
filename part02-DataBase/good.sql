-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: db_good
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_author`
--

create database db_good;

use db_good;

DROP TABLE IF EXISTS `t_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_author` (
  `author_id` int NOT NULL AUTO_INCREMENT,
  `author_name` char(100) DEFAULT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_author`
--

LOCK TABLES `t_author` WRITE;
/*!40000 ALTER TABLE `t_author` DISABLE KEYS */;
INSERT INTO `t_author` VALUES (1,'tom'),(2,'jerry');
/*!40000 ALTER TABLE `t_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_banana`
--

DROP TABLE IF EXISTS `t_banana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_banana` (
  `banana_id` int DEFAULT NULL,
  `banana_age` int NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `banana_age` (`banana_age`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_banana`
--

LOCK TABLES `t_banana` WRITE;
/*!40000 ALTER TABLE `t_banana` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_banana` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_book`
--

DROP TABLE IF EXISTS `t_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_book` (
  `book_price` double DEFAULT NULL,
  `book_grade` char(100) DEFAULT NULL,
  `book_id` int NOT NULL AUTO_INCREMENT,
  `author_id` int DEFAULT NULL,
  `book_name` char(100) DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `book_name` (`book_name`),
  UNIQUE KEY `book_grade` (`book_grade`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `t_book_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `t_author` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_book`
--

LOCK TABLES `t_book` WRITE;
/*!40000 ALTER TABLE `t_book` DISABLE KEYS */;
INSERT INTO `t_book` VALUES (55,'1',1,1,'new name'),(11.11,'2',3,1,'name01'),(12.11,'3',4,1,'name02'),(13.11,'4',5,1,'name03');
/*!40000 ALTER TABLE `t_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_hire`
--

DROP TABLE IF EXISTS `t_hire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_hire` (
  `hire_id` int NOT NULL AUTO_INCREMENT,
  `hire_date` date DEFAULT NULL,
  `employee_birthday` date DEFAULT NULL,
  PRIMARY KEY (`hire_id`),
  CONSTRAINT `t_hire_chk_1` CHECK (((year(`hire_date`) - year(`employee_birthday`)) > 18))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_hire`
--

LOCK TABLES `t_hire` WRITE;
/*!40000 ALTER TABLE `t_hire` DISABLE KEYS */;
INSERT INTO `t_hire` VALUES (1,'2023-06-29','2000-03-05');
/*!40000 ALTER TABLE `t_hire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_master`
--

DROP TABLE IF EXISTS `t_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_master` (
  `master_id` int NOT NULL AUTO_INCREMENT,
  `master_name` char(100) DEFAULT NULL,
  PRIMARY KEY (`master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_master`
--

LOCK TABLES `t_master` WRITE;
/*!40000 ALTER TABLE `t_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_my_try`
--

DROP TABLE IF EXISTS `t_my_try`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_my_try` (
  `try_id` int NOT NULL AUTO_INCREMENT,
  `try_name` char(100) DEFAULT NULL,
  PRIMARY KEY (`try_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_my_try`
--

LOCK TABLES `t_my_try` WRITE;
/*!40000 ALTER TABLE `t_my_try` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_my_try` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_orange`
--

DROP TABLE IF EXISTS `t_orange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_orange` (
  `orange_id` int DEFAULT NULL,
  `orange_age` int NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `orange_age` (`orange_age`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_orange`
--

LOCK TABLES `t_orange` WRITE;
/*!40000 ALTER TABLE `t_orange` DISABLE KEYS */;
INSERT INTO `t_orange` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `t_orange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pig`
--

DROP TABLE IF EXISTS `t_pig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_pig` (
  `pig_id` int NOT NULL AUTO_INCREMENT,
  `pig_name` char(100) NOT NULL DEFAULT '浣╁',
  `pig_age` int DEFAULT NULL,
  `pig_weight` int DEFAULT NULL,
  PRIMARY KEY (`pig_id`),
  UNIQUE KEY `pig_name` (`pig_name`),
  UNIQUE KEY `pig_age` (`pig_age`,`pig_weight`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pig`
--

LOCK TABLES `t_pig` WRITE;
/*!40000 ALTER TABLE `t_pig` DISABLE KEYS */;
INSERT INTO `t_pig` VALUES (1,'aaa',10,33),(2,'bb',33,66),(3,'ccc',39,22);
/*!40000 ALTER TABLE `t_pig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_school`
--

DROP TABLE IF EXISTS `t_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_school` (
  `school_id` int NOT NULL AUTO_INCREMENT,
  `school_name` char(100) NOT NULL,
  `school_age` int DEFAULT NULL,
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_school`
--

LOCK TABLES `t_school` WRITE;
/*!40000 ALTER TABLE `t_school` DISABLE KEYS */;
INSERT INTO `t_school` VALUES (1,'atguigu',25);
/*!40000 ALTER TABLE `t_school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_slave`
--

DROP TABLE IF EXISTS `t_slave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_slave` (
  `slave_id` int NOT NULL AUTO_INCREMENT,
  `slave_name` char(100) DEFAULT NULL,
  `master_id` int DEFAULT NULL,
  PRIMARY KEY (`slave_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_slave`
--

LOCK TABLES `t_slave` WRITE;
/*!40000 ALTER TABLE `t_slave` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_slave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_student`
--

DROP TABLE IF EXISTS `t_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_student` (
  `stu_id` int NOT NULL AUTO_INCREMENT,
  `stu_name` char(100) NOT NULL DEFAULT 'tom',
  `stu_age` int DEFAULT '20',
  PRIMARY KEY (`stu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_student`
--

LOCK TABLES `t_student` WRITE;
/*!40000 ALTER TABLE `t_student` DISABLE KEYS */;
INSERT INTO `t_student` VALUES (1,'jerry',NULL),(2,'tom',500);
/*!40000 ALTER TABLE `t_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_teacher`
--

DROP TABLE IF EXISTS `t_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_teacher` (
  `teacher_id` int NOT NULL AUTO_INCREMENT,
  `teacher_name` char(100) DEFAULT NULL,
  `teacher_subject` char(100) DEFAULT NULL,
  `teacher_direct` char(100) DEFAULT NULL,
  PRIMARY KEY (`teacher_id`),
  UNIQUE KEY `teacher_name` (`teacher_name`),
  UNIQUE KEY `teacher_subject` (`teacher_subject`,`teacher_direct`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_teacher`
--

LOCK TABLES `t_teacher` WRITE;
/*!40000 ALTER TABLE `t_teacher` DISABLE KEYS */;
INSERT INTO `t_teacher` VALUES (1,'aaa','Java','Test'),(3,NULL,'Java','Development'),(4,NULL,'MySQL','Development');
/*!40000 ALTER TABLE `t_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_tiger`
--

DROP TABLE IF EXISTS `t_tiger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_tiger` (
  `tiger_id` int NOT NULL,
  `tiger_name` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_tiger`
--

LOCK TABLES `t_tiger` WRITE;
/*!40000 ALTER TABLE `t_tiger` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_tiger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user`
--

DROP TABLE IF EXISTS `t_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_age` int DEFAULT NULL,
  `user_name` char(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `t_user_chk_1` CHECK ((`user_age` between 18 and 25))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user`
--

LOCK TABLES `t_user` WRITE;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` VALUES (1,23,'tom');
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_worker`
--

DROP TABLE IF EXISTS `t_worker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_worker` (
  `worker_id` int NOT NULL AUTO_INCREMENT,
  `worker_gender` enum('male','female','secret') DEFAULT NULL,
  `worker_subject` set('Java','MySQL','PHP','Thread','Redis') DEFAULT NULL,
  `worker_salary` double(10,3) DEFAULT NULL,
  PRIMARY KEY (`worker_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_worker`
--

LOCK TABLES `t_worker` WRITE;
/*!40000 ALTER TABLE `t_worker` DISABLE KEYS */;
INSERT INTO `t_worker` VALUES (1,'male','Java,MySQL,PHP',NULL),(2,'female','Java,MySQL,PHP',NULL),(3,'female','Java,PHP,Thread',NULL),(4,'male','Java',1000.556);
/*!40000 ALTER TABLE `t_worker` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-29 16:45:34

create database db_hr;

use db_hr;

create table t_emp(
    emp_id int auto_increment primary key ,
    emp_name char(100),
    emp_salary double,
    emp_subject char(100),
    emp_gender char(100),
    emp_age int,
    dept_id int,    # 当前员工所在部门的 id
    manager_id int  # 当前员工的主管在 t_emp 表中的 id
);

create table t_dept(
    dept_id int auto_increment primary key ,
    dept_name char(100)
);
INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (1, 'John Smith', 5000, 'Mathematics', 'Male', 30, 1, 3);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (2, 'Emily Johnson', 4500, 'English', 'Female', 28, 1, 3);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (3, 'Michael Miller', 6000, 'Physics', 'Male', 35, 2, 5);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (4, 'Sophia Davis', 5500, 'Chemistry', 'Female', 32, 2, 5);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (5, 'William Wilson', 4000, 'History', 'Male', 26, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (6, 'Emma Thompson', 4800, 'Biology', 'Female', 29, 2, 5);

# 没有部门的员工
INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (7, 'James Anderson', 5200, 'Computer Science', 'Male', 31, NULL, NULL);

# 没有部门的员工
INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (8, 'Olivia Harris', 4700, 'French', 'Female', 27, NULL, NULL);

# 没有部门的员工
INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (9, 'Daniel Martinez', 5500, 'Economics', 'Male', 33, NULL, NULL);

INSERT INTO t_dept (dept_id, dept_name)
VALUES (1, 'Human Resources');

INSERT INTO t_dept (dept_id, dept_name)
VALUES (2, 'Finance');

INSERT INTO t_dept (dept_id, dept_name)
VALUES (3, 'Marketing');

# 没有员工的部门
INSERT INTO t_dept (dept_id, dept_name)
VALUES (4, 'Quality Assurance');

# 没有员工的部门
INSERT INTO t_dept (dept_id, dept_name)
VALUES (5, 'Administration');

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (10, 'Ava Phillips', 5200, 'Geography', 'Female', 30, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (11, 'Benjamin Clark', 4900, 'Chemistry', 'Male', 28, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (12, 'Mia Lewis', 5300, 'English', 'Female', 32, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (13, 'Henry Walker', 4600, 'Mathematics', 'Male', 26, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (14, 'Ella Hill', 5100, 'Physics', 'Female', 29, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (15, 'Alexander Adams', 4900, 'History', 'Male', 31, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (16, 'Grace Baker', 5400, 'Computer Science', 'Female', 27, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (17, 'Sebastian Green', 4800, 'Spanish', 'Male', 33, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (18, 'Chloe Edwards', 5200, 'Biology', 'Female', 30, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (19, 'Samuel Turner', 4700, 'Mathematics', 'Male', 28, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (20, 'Zoe Collins', 5100, 'Chemistry', 'Female', 32, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (21, 'Liam Walker', 4600, 'History', 'Male', 29, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (22, 'Lily Hall', 4900, 'English', 'Female', 31, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (23, 'Noah Nelson', 5300, 'Mathematics', 'Male', 27, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (24, 'Aria Carter', 4700, 'Physics', 'Female', 33, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (25, 'William Stewart', 5100, 'Chemistry', 'Male', 30, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (26, 'Harper Murphy', 4800, 'Computer Science', 'Female', 28, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (27, 'James King', 5200, 'French', 'Male', 32, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (28, 'Evelyn Price', 4600, 'Biology', 'Female', 26, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (29, 'Oliver Wood', 5000, 'Geography', 'Male', 29, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (30, 'Amelia Bell', 4400, 'Computer Science', 'Female', 31, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (31, 'Lucas Mitchell', 4800, 'Economics', 'Male', 27, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (32, 'Mia Turner', 5300, 'Spanish', 'Female', 33, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (33, 'Henry White', 4700, 'Mathematics', 'Male', 30, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (34, 'Charlotte Harris', 5100, 'Physics', 'Female', 28, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (35, 'Alexander Clark', 4900, 'Chemistry', 'Male', 32, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (36, 'Scarlett Wright', 5200, 'English', 'Female', 26, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (37, 'Daniel Young', 4600, 'Mathematics', 'Male', 29, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (38, 'Sophia Ward', 4900, 'History', 'Female', 31, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (39, 'Jack Foster', 5300, 'English', 'Male', 27, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (40, 'Ava Richardson', 4700, 'Mathematics', 'Female', 33, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (41, 'Logan Morgan', 5100, 'Physics', 'Male', 30, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (42, 'Mia Anderson', 4800, 'Computer Science', 'Female', 28, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (43, 'Lucas Cooper', 5200, 'French', 'Male', 32, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (44, 'Isabella Reed', 4600, 'Biology', 'Female', 26, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (45, 'Mason Coleman', 5000, 'Geography', 'Male', 29, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (46, 'Olivia Richardson', 4500, 'Computer Science', 'Female', 31, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (47, 'Ethan Peterson', 5200, 'Economics', 'Male', 27, 3, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (48, 'Emma Jackson', 5300, 'Spanish', 'Female', 33, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (49, 'William Jenkins', 4700, 'Mathematics', 'Male', 30, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (50, 'Ava Wright', 5100, 'Physics', 'Female', 28, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (51, 'James Davis', 4900, 'Chemistry', 'Male', 32, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (52, 'Charlotte Evans', 5200, 'English', 'Female', 26, 1, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (53, 'Daniel Brown', 4600, 'Mathematics', 'Male', 29, 2, NULL);

INSERT INTO t_emp (emp_id, emp_name, emp_salary, emp_subject, emp_gender, emp_age, dept_id, manager_id)
VALUES (54, 'Amelia Wilson', 5000, 'History', 'Female', 31, 1, NULL);